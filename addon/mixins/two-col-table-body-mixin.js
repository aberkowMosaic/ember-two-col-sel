import { A } from '@ember/array';
import Mixin from '@ember/object/mixin';

export default Mixin.create({
  init() {
    this._super(...arguments);
    this.set('mouseDragData.dragging', false);

    var options = {
      closeButton: false,
      tapToDismiss: false,
      onclick: false,
      closeOnHover: false,
      hideMethod: 'hide',
      showMethod: 'show'
    };

    this.set('toastOptions', options);
  },
  tagName: 'div',
  firstId: 0,
  firstRow: 0,
  canAssignMore: true,
  currentNumberAvailableToAssign: null,

  mouseDown: function(event) {
    if(event.which !== 1 || event.altKey || event.ctrlKey || event.shiftKey || $(event.target).data().disabled ) {
      return;
    }
    this.set('mouseDragData.dragging', true);
    var uid = $(event.target).data().uid;
    var row = parseInt($(event.target).attr('data-row'));
    this.setProperties({firstId: uid, firstRow: row});
    this.get('mouseDragData.selectedIds').push($(event.target).data());
    this.get('mouseDragData.selectedRows').push(row);
    $(event.target).parent().addClass("selected");
  },
  mouseUp: function() {
    if(this.get('mouseDragData.dragging')){
      var lastUid = this.get('mouseDragData.selectedIds')[this.get('mouseDragData.selectedIds').length - 1].uid

      var ids = this.get('mouseDragData.selectedIds').mapBy('uid');
      //if there is a limit on how manay can be assigned, cap the ids to highlight at that number
      if(this.get('currentNumberAvailableToAssign')){
        if(this.get('currentNumberAvailableToAssign') < ids.length){
          this.showMaxAssignableWarning();
          ids = ids.slice(0, this.get('currentNumberAvailableToAssign'));
        }
      }

      this.get('data').forEach(function(item){
        if(ids.indexOf(parseInt(item.get('id'))) >= 0){
          //check if the user has reached the limit on how many can be assigned
          if(this.get('canAssignMore')){
            if(!item.get('disabled')){
              item.set('checked', !item.get('checked'));
            }
          }else{
            //still allow user to uncheck any items
            if(item.get('checked')){
              item.set('checked', !item.get('checked'));
              this.toast.clear()
            }else{
              this.showMaxAssignableWarning();
            }
          }

        }
      }.bind(this));
      this.clearSelection();
    }
    return true;
  },
  mouseMove: function(event){


    if(this.get('mouseDragData.dragging') ){
      if('row' in $(event.target).data()){
        var row = parseInt($(event.target).attr("data-row"));
        var side = $(event.target).data().side;

        var selectedIdsRecomputed = [];
        var selectedRowsRecomputed = [];
        //re create both arrays by grabbing the first item in list and getting silbling until we get this one and re contruct arrays
        var firstRow = this.get('firstRow');
        var i, currentDiv;
        if(row > firstRow){
          //dragging down
          for(i=firstRow; i <= row; i++){
            currentDiv = $(event.target).closest('.two-col-select-wrapper').find('.two-col-select-item[data-row='+i+'][data-side="'+side+'"]');
            selectedIdsRecomputed.push(currentDiv.data());
            selectedRowsRecomputed.push(parseInt(currentDiv.attr("data-row")));
          }
        }else{
          //dragging up
          for(i=firstRow; i >= row; i--){
            currentDiv = $(event.target).closest('.two-col-select-wrapper').find('.two-col-select-item[data-row="'+i+'"][data-side="'+side+'"]');
            selectedIdsRecomputed.push(currentDiv.data());
            selectedRowsRecomputed.push(parseInt(currentDiv.attr("data-row")));
          }
        }

        //set the arrays to new values that will include any that were skipped over
        this.set('mouseDragData.selectedIds', selectedIdsRecomputed);
        this.set('mouseDragData.selectedRows', selectedRowsRecomputed);

      }
    }
  },
  clearSelection(){
    " I was called"
    $('.selected').removeClass('selected');
    this.set('mouseDragData', {
      'selectedRows': A(),
      'selectedIds': A(),
      'dragging': false
    });
  },

  showMaxAssignableWarning: function(){
    this.toast.error(this.get('maxAssignableMsg'), 'Warning', this.get('toastOptions'));
  },

  actions: {
    moveItems: function(){
      this.sendAction('moveItemsUp', this.get('mouseDragData.selectedIds'));
    }
  }
});
