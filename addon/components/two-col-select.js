import { computed } from '@ember/object';
import Component from '@ember/component';
import { A } from '@ember/array';
import { inject as service } from '@ember/service';
import TwoColSelectData from '../utils/two-col-select-data';
import TwoColSelectFilters from '../utils/two-col-select-filters';
import { runTask, runDisposables } from 'ember-lifeline';
import moment from 'moment';

export default Component.extend( {
  init() {
    this._super(...arguments);
    let rightColData = TwoColSelectData.create();
    // ensures not objects that are destoryed somehow to into list
    this.set('rightTableDataList', this.get('rightTableDataList').filterBy('isDestroyed', false));
    rightColData.loadArray(this.get('rightTableDataList'));
    this.set('rightTableData', rightColData);
    this.get('rightTableData').getList().forEach(function(item){
      item.set('checked', false);
    })

    let leftColData = TwoColSelectData.create();

    // ensures not objects that are destroyed somehow to into list
    if (this.get('leftTableDataList')) {
      this.set('leftTableDataList', this.get('leftTableDataList').filterBy('isDestroyed', false));
      leftColData.loadArray(this.get('leftTableDataList'));
    }

    this.set('leftTableData', leftColData);
    this.get('leftTableData').getList().forEach(function(item){
      item.set('checked', false);
    });
    //right col data needs to be set in init() of component since this can take differnt forms.

    let leftTableFilter = TwoColSelectFilters.create();
    leftTableFilter.loadFilters(this.get('leftTableFilterList'));
    this.set('leftTableFilter', leftTableFilter);

    let rightTableFilter = TwoColSelectFilters.create();
    rightTableFilter.loadFilters(this.get('rightTableFilterList'));
    this.set('rightTableFilter', rightTableFilter);

    if(this.get('rightTableTabFilterList') !== undefined){
      let rightTableTabFilter = TwoColSelectFilters.create();
      rightTableTabFilter.loadFilters(this.get('rightTableTabFilterList'));
      this.set('rightTableTabFilter', rightTableTabFilter);
    }

    $('.right-col-header').height($('.left-col-header').height());

    this.set('leftMouseDragData',{
      'selectedRows': A(),
      'selectedIds': A(),
      'dragging': false
    });

    this.set('rightMouseDragData',{
      'selectedRows': A(),
      'selectedIds': A(),
      'dragging': false
    });

    this.set('filterByLeft', []);
    this.set('filterByRight', []);

    this.set('rightTableFilterList', A([]));
    this.set('leftTableFilterList', A([]));
    this.set('rightTableTabFilterList', A([]));
  },

  didRender(){
    this._super(...arguments);
    let twoColSelectId = this.getWithDefault('twoColSelectId', 'twoColSelect');
    $('#'+twoColSelectId).find('.right-col-header').height($('#'+twoColSelectId).find('.left-col-header').height());
    runTask(this, () => {
      $('#'+twoColSelectId).find('.right-col-header').height($('#'+twoColSelectId).find('.left-col-header').height());
    }, 100);
  },

  willDestroy() {
    this._super(...arguments);

    runDisposables(this);
  },

  /* Defaults */
  store: service('store'),
  removeFromLeft: true, // Allow items to be removed from the left side
  removeFromRight: true, // Allow items to eb removed from the right side
  grouping: false, // Group items by a field
  groupByColumn: '', // Which field to group items by, if grouping is enabled
  rowLabel: "name",
  filterByLeft: null,
  filterByRight: null,
  applyAutoFilter: false,
  showFilters: false,
  sortKey: "name",
  customData: null,
  somethingMoved: false,
  hasFilter: true,
  rightTableFilterList: null,
  leftTableFilterList: null,
  contextualFilterChanged: false,//set to true if a contextual multi select changing triggers filtering
  leftAllChecked: false,
  rightAllChecked: false,
  leftMouseDragData: null,
  rightMouseDragData: null,
  maxAssignable: null,

  rightGrouping: computed('grouping', 'rightGrouped', function(){
    return this.get('grouping') || this.get('rightGrouped');
  }),
  filteredLeftTableData: computed( 'leftTableFilter.filters.@each.selectedItemsLength', 'leftTableFilter.filters.@each.dateFrom',
   'leftTableFilter.filters.@each.dateTo', 'leftTableFilter.filters.@each.filterCheckbox','leftTableData.list.length', 'leftTableData',
   'leftTableFilter.filters.@each.searchValue',function(){
    let filteredResults =  this.applyColumnFilters(this.get('leftTableData').getList(), this.get('leftTableFilter').getFilters());

    if(!this.get('contextualFilterChanged'), this.get('leftTableFilter').getFilters().filterBy('contextual', true).length > 0){
      this.computeContextualFilters(this.get('leftTableData').getList(), this.get('leftTableFilter').getFilters());
    }

    this.set('contextualFilterChanged', false);
    return filteredResults;
  }),
  filteredRightTableData: computed('rightTableFilter.filters.@each.selectedItemsLength', 'rightTableFilter.filters.@each.dateFrom',
   'rightTableFilter.filters.@each.dateTo', 'rightTableFilter.filters.@each.filterCheckbox','rightTableData.list.length', 'rightTableData',
   'rightTableFilter.filters.@each.searchValue', function(){
   let filteredResults =  this.applyColumnFilters(this.get('rightTableData').getList(), this.get('rightTableFilter').getFilters());

   if(!this.get('contextualFilterChanged'), this.get('rightTableFilter').getFilters().filterBy('contextual', true).length > 0){
     this.computeContextualFilters(this.get('rightTableData').getList(), this.get('rightTableFilter').getFilters());
   }

   this.set('contextualFilterChanged', false);
   return filteredResults;
  }),

  filtertedRightTableTabData: computed('rightTableTabFilter.filters.@each.selectedItemsLength', 'rightTableTabFilter.filters.@each.dateFrom',
   'rightTableTabFilter.filters.@each.dateTo', 'rightTableTabFilter.filters.@each.filterCheckbox','rightColumnTabData.rightTabList',
   'rightTableTabFilter.filters.@each.searchValue', function(){
   if(this.get('rightTableTabFilter') !== undefined){
     let filteredResults =  this.applyColumnFilters(this.get('rightColumnTabData.rightTabList'), this.get('rightTableTabFilter').getFilters());

     if(!this.get('contextualFilterChanged'), this.get('rightTableTabFilter').getFilters().filterBy('contextual', true).length > 0){
       this.computeContextualFilters(rightColumnTabData.rightTabList, this.get('rightTableTabFilter').getFilters());
     }

     this.set('contextualFilterChanged', false);
     return filteredResults;
   }
   return this.get('rightColumnTabData.rightTabList');
  }),

  currentLeftColResults: computed('filteredLeftTableData.length', function(){
    return this.get('filteredLeftTableData').length;
  }),
  totalLeftColResults: computed('leftTableData.list.length', function(){
    return this.get('leftTableData').getList().length;
  }),
  currentRightColResults: computed('filteredRightTableData.length', function(){
    return this.get('filteredRightTableData').toArray().length;
  }),
  totalRightColResults: computed('rightTableData.list.length', function(){
    return this.get('rightTableData').getList().toArray().length;
  }),
  leftSideEmpty: computed('currentLeftColResults', function(){
    return this.get('currentLeftColResults') === 0;
  }),
  rightSideEmpty: computed('totalRightColResults', function(){
    return this.get('currentRightColResults') === 0;
  }),

  currentNumberToAssign: computed('maxAssignable', 'totalRightColResults', 'filteredLeftTableData.@each.checked', function() {
    if(this.get('maxAssignable')){
      return this.get('totalRightColResults') + this.get('leftTableData').getList().filterBy('checked', true).length;
    }
    return null;
  }),

  currentNumberAvailableToAssign: computed('maxAssignable', 'currentNumberToAssign',function() {
    return this.get('maxAssignable') - this.get('currentNumberToAssign');
  }),

  canAssignMore: computed('maxAssignable', 'currentNumberToAssign', 'currentNumberAvailableToAssign', function() {
    if(this.get('maxAssignable')){
      if(this.get('currentNumberAvailableToAssign') <= 0){
        var options = {
          closeButton: false,
          preventDuplicates: true,
          timeOut: 0,
          tapToDismiss: false,
          onclick: false,
          closeOnHover: false
        }
        if(this.get('currentNumberAvailableToAssign') < 0) {
          this.toast.error(this.get('maxAssignableMsg'), 'Warning', options);
        }
        return false;
      }
    }
    return true;
  }),

  hideCheckAllLeft: computed('canAssignMore', function() {
    if(this.get('maxAssignable')){
      return this.get('maxAssignable') - this.get('leftTableData').getList().length < 0;
    }
    return false;
  }),

  currentLeftColResults: computed('filteredLeftTableData.length', function(){
    return this.get('filteredLeftTableData').toArray().length;
  }),
  totalLeftColResults: computed('leftTableData.list.length', function(){
    return this.get('leftTableData').getList().toArray().length;
  }),

  computeContextualFilters: function(data, columnFilters){
    var contextualFilters = columnFilters.filterBy('contextual', true);//@todo make get list and foreach overthem
    contextualFilters.forEach(function(contextualFilter) {
      // get all results that would be available without the contextual filters being applied. At the current time we do not cross contextual filters.
      let filteredResultsWithoutThisFilter = this.applyColumnFilters(data, columnFilters.filterBy('contextual', false));

      var uniqFilterResultsContent;
      //@TODO - break our for differnt cases depending on depth level.;
      if(contextualFilter.get('filterTargetDepth') === 1){
        uniqFilterResultsContent = filteredResultsWithoutThisFilter.mapBy(contextualFilter.get('filterTarget') + '.' + contextualFilter.get('filterTargetVar')).uniq();
      }

      //get all the possible filter options as defined in filter. Filter b uniq contextual results
      let contextualContent = this.get('store').peekAll(contextualFilter.get('contextualObject')).toArray();
      contextualContent = contextualContent.filter(function(item){
        return uniqFilterResultsContent.includes(item.get('name'));
      });

      let currentContent = contextualFilter.get('filterSelectContent');

      // get objects to remove from current content
      var contentItemsToRemove = currentContent.filter(function(item){
        return !contextualContent.includes(item);
      });

      // get objects to add to current
      var contentItemsToAdd = contextualContent.filter(function(item){
        return !currentContent.includes(item);
      });

      // use addObject and removeObject to trigger selectize's observers
      contentItemsToAdd.forEach(function(item) {
        contextualFilter.get('filterSelectContent').addObject(item);
      });

      contentItemsToRemove.forEach(function(item) {
        contextualFilter.get('filterSelectContent').removeObject(item);
      });

    }.bind(this));
  },
  applyColumnFilters: function(data, columnFilters){

    if(columnFilters && Array.isArray(columnFilters)){
      //loop through each filter
      columnFilters.forEach(function(filter){
        //conditional for selectize
         if (filter.type === "multi-select") {
           data = this.applyMultiSelectFilter(data, filter);
         }else if(filter.type === "checkbox"){
           data = this.applyCheckboxFilter(data, filter);
         }else if(filter.type === "date-range"){
           data = this.applyDateRangeFilter(data, filter);
         }else if(filter.type === "text"){
           data = this.applyTextFilter(data, filter);
         }
      }.bind(this));
    }

    //set index's to rows for drag select
    if(this.get('grouping')){
      data.sortBy(this.get('groupByColumn'), this.get('sortKey')).forEach(function(item, index) {
        item.set('index', index);
      });
    }else{
      data.sortBy(this.get('sortKey')).forEach(function(item, index) {
        item.set('index', index);
      });
    }

    return data;
  },
  applyMultiSelectFilter: function(data, filter){
    if(filter && Array.isArray(filter.selectedItems) && filter.selectedItems.length){
      var filterTarget = filter.filterTarget;
      var filterTargetDepth = filter.filterTargetDepth;
      var filterTargetVar = filter.filterTargetVar;
      var filterBy = filter.selectedItems;

      if (!filterTarget || !filterBy) {
        return data;
      }

      data = data.filter(function(item){
        var includeInFilter = false;

        //filterTargetDepth is 0 for just looking at string.
        if(filterTargetDepth === 0){
          //for just looking at string.
          let entry = item.get(filterTarget);

          filterBy.forEach(function(filterOption) {
             if(typeof entry !== undefined && (filterOption.value === entry)){
              includeInFilter =  true;
            }
          });
        //filterTargetDepth is 1 if were looking at the variable of an object,
        }else if (filterTargetDepth === 1 && filterTargetVar) {
          let entry = item.get(filterTarget);
          filterBy.forEach(function(filterOption) {
            if(entry && typeof entry !== undefined && entry != null && (filterOption.get(filterTargetVar) === entry.get(filterTargetVar))){
              includeInFilter =  true;
            }
          });
        //filterTargetDepth is 2 if were are looking at an array of objects and looking for at least one match.
        }else if (filterTargetVar){
          item.get(filterTarget).forEach(function(entry) {
            //loop through each selected item in this filter by the object were are targeting
            filterBy.forEach(function(filterOption) {
              if(filterOption.get(filterTargetVar) === entry.get(filterTargetVar)){
                includeInFilter =  true;
              }
            });
          });
        }
        return includeInFilter;
      });
    }
    return data;
  },
  applyCheckboxFilter: function(data, filter){

    return data.filter(function(item){
      var includeInFilter = true;
      if(filter.filterActionSwitch === filter.filterCheckbox){

        if(item.get(filter.filterTargetVar) !== filter.filterTarget){

          includeInFilter = false;
        }
      }
      return includeInFilter;
    });
  },
  applyDateRangeFilter: function(data, filter){
    var from = filter.dateFrom ? moment(filter.dateFrom).unix() : 0;
    var to = filter.dateTo ? moment(filter.dateTo).unix() : 0;
    return data.filter(function(item){
      var includeInFilter = false;
      let targetDate = item.get(filter.filterTargetVar);
      if (isNaN(targetDate)) {
        targetDate = moment(targetDate).unix();
      }
      if( (targetDate >= from || from === 0) && (targetDate <= to || to === 0)){
        includeInFilter = true;
      }
      return includeInFilter;
    });
  },
  applyTextFilter: function(data, filter){
    return data.filter(function(item){
      var includeInFilter = false;

      if(filter.searchValue === null){
        includeInFilter = true;
      }else{
        filter.filterTarget.forEach(function(target){
          if(item.get(target).toLowerCase().indexOf(filter.searchValue.toLowerCase()) !== -1){
            includeInFilter = true;
          }
        });
      }
      return includeInFilter;
    });
  },
  moveItems: function(ids, sourceSide, targetSide){
    this.sendAction('moveItems', ids, sourceSide, targetSide);
  },
  actions: {
      updateSelectFilter: function(filter){
        if(filter.get('contextual')){
          this.set('contextualFilterChanged', true);
        }
      },

      move(){
        var leftItems = this.get('leftTableData').getList().filterBy('checked', true);
        var rightItems = this.get('rightTableData').getList().filterBy('checked', true);
        //move from left to right
        this.send("moveItems", leftItems, "left", "right");
        this.moveItems(leftItems,"left", "right");

        //move from right to left
        this.send("moveItems", rightItems, "right", "left");
        this.moveItems(rightItems, "right", "left");

        this.setProperties({
          leftAllChecked: false,
          rightAllChecked: false
        });
      },
      moveItems: function(itemsToMove, sourceSide){
        var source, dest;
        if(sourceSide === "left"){
          source = this.get('leftTableData');
          dest = this.get('rightTableData');
        }else{
          source = this.get('rightTableData');
          dest = this.get('leftTableData');
        }
        itemsToMove.forEach(function(item){
          dest.addObject(item);
          source.removeObject(item);
          item.set('checked', false);
        }.bind(this));
      }
  }//end actions


});
