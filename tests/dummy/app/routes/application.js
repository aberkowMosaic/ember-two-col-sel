import Route from '@ember/routing/route';
import RSVP from 'rsvp';
import { A } from '@ember/array';
import EmberObject from '@ember/object';
import faker from 'faker';
import $ from 'jquery';

export default Route.extend({

  model(){
    let leftFilters = this.buildFilter();
    var leftTableDataList = A ([]);
    var rightTableDataList = A ([]);
    var object = EmberObject.extend({});

    for(let i=0; i< 100; i++){
      var rowData = object.create();
      rowData.set("id",i);
      rowData.set("name",faker.name.firstName());
      rowData.set("place","home");
      rowData.set("disabled",false);

      if(i % 4 === 0){
        rowData.set("disabled",true);
      }



      if(i <= 49){
        rightTableDataList.push(rowData);
      }
      else{
        leftTableDataList.push(rowData);
      }

  }

    return RSVP.hash({
      leftTableDataList:leftTableDataList,
      rightTableDataList:rightTableDataList,
      leftFilters: leftFilters
    });

  },

  buildFilter: function(){
  var userCategoryFilter = {
      type: 'multi-select',
      label: 'User Type',
      name: 'place',
      filterSelectContent: [
        {'value' : 'work', 'name': 'Work'},
        {'value' : 'home', 'name': 'Home'},

      ],
      filterTarget: 'place',
      filterTargetVar: 'place',
      filterTargetDepth: 0,
      selectedItems: [],
    };


    var nameFilter = {
      type: 'text',
      label: 'Name',
      name: 'name',
      filterTarget: ['name'],
      searchValue: null
    };

    var disabledFilter =  {
            type: 'checkbox',
            name: 'disabled',
            label: 'Disabled',
            filterTargetVar: 'disabled',
            filterActionSwitch: true,//true means compute filter on checked, otherwise show all. false means compute filter on unchecked, otherwise show all.
            filterCheckbox: false,
            filterTarget: true //value you are checking for to show if filterActionSwitch == filterCheckbox
          };

    return [
      JSON.parse(JSON.stringify(userCategoryFilter)),
      JSON.parse(JSON.stringify(disabledFilter)),
      nameFilter
    ];
  }


});
