import Controller from '@ember/controller';
import TwoColMoveMixin from 'ember-two-col-select/mixins/two-column-select-move-items-mixin';
import { computed } from '@ember/object';

export default Controller.extend(TwoColMoveMixin,{
    leftColumnTitle: 'Available Courses',
    rightColumnTitle: 'Selected Courses',
    twoColLeftSideData: 'model.leftTableDataList',
    twoColRightSideData: 'model.rightTableDataList',
    twoColSelectLabels: null,
    leftFilters: [],
    rightFilters: [],
    sortKey: "name"


});
